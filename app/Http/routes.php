<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
	'as' => 'home', 
	'uses' =>'PagesController@home'

	]);  


Route::group(['middleware' => ['web']], function () {

Route::get('inregistrare', [
			'as' => 'get_inregistrare', 
			'uses' => 'PagesController@getInregistrare'
	]);  
Route::post('inregistrare-post', [
			'as' => 'post_inregistrare', 
			'uses' => 'PagesController@postInregistrare'
	]);  

Route::get('lista', [
			'as' => 'lista', 
			'uses' => 'PagesController@getLista'
	]);   
Route::get('deleteUser/{id}',[
			'as' => 'deleteUser', 
			'uses' => 'PagesController@destroy'
	

	]); 


Route::get('editUser/{id}', [
			'as' => 'getEditUser', 
			'uses' => 'PagesController@getEditUser' 

			]); 

Route::post('editUser/{id}', [
			'as' => 'postEditUser', 
			'uses' => 'PagesController@postEditUser'
	

	]);   

Route::get('interfataselectare', [
	'as' => 'interfata_selectare', 
	'uses' =>'PagesController@getButoane'

	]);   

Route::get('calculator', [
	'as' => 'calculator', 
	'uses' =>'CalculatorController@getCalculator'

	]);   
Route::get('insertcalculator', [
	'as' => 'get_insertcalculator', 
	'uses' =>'CalculatorController@getInsertCalculator'

	]);   
Route::post('insertcalculator', [
	'as' => 'post_insertcalculator', 
	'uses' =>'CalculatorController@postInsertCalculator'

	]);   
Route::get('editCalculator/{id}', [
	'as' => 'getEditCalculator', 
	'uses' => 'CalculatorController@getEditCalculator'	

	]); 
Route::post('editCalculator/{id}', [
	'as' => 'postEditCalculator', 
	'uses' => 'CalculatorController@postEditCalculator'	

	]); 
Route::get('pictureupload' , [
	'as' => 'pictureupload' , 
	'uses' => 'UploadController@getUploadForm'
	]); 
Route::post('profilepicture/{id}', [
	'as' => 'profilepicture' , 
	'uses' => 'UploadController@postUploadForm'

	]);

Route::get('profile/{id}', [
	'as' => 'profile', 
	'uses' => 'PagesController@getProfile'

	]);

});