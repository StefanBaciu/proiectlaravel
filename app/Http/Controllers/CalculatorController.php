<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests; 
use App\Calculator; 
use App\Utilizator;

class CalculatorController extends Controller
{
    public function getCalculator(Request $request) 
    {
        $utilizatori = Utilizator::lists('firstname','id')->toArray(); 
        $calculatoare = Calculator::whereRaw('1 = 1'); 

        $user_id = null;

        if($request->has('user_id')) { 

        $user_id = $request->input('user_id'); 
        $calculatoare->where('user_id',$user_id);  

        }

        $calculatoare = $calculatoare->get();

        return view('pages.calculator', compact('utilizatori','calculatoare'));

    } 

    public function getInsertCalculator() 
    {
    	return view('pages.insertcalculator');
    }

    public function postInsertCalculator(Request $request ) 
    { 
    	
    	$this->validate($request, [
    		'model'=>'required', 
    		'tip'=>'required', 
    		'data_receptie'=>'required|date_format:Y-m-d'
    		]);


    	$calculator = new Calculator; 
    	$calculator->model = $request->model;
    	$calculator->tip = $request->tip;
    	$calculator->data_receptie = $request->data_receptie; 


    	$calculator->save();  


    	return redirect()->route('calculator')->with('Success','Calculator adaugat');
    } 

    public function getEditCalculator(Request $request, $id) 
    {
        $calculator = Calculator::findOrFail($id); 
        return view('pages.editcalculator', compact('calculator'));

    } 

    public function postEditCalculator(Request $request, $id)
    { 
        $this->validate($request, [
                'model' => 'required', 
                'tip' => 'required', 
                'data_receptie' => 'required|date_format:Y-m-d'
            ]);

        $calculator = Calculator::findOrFail($id); 
        $calculator->model = $request->model; 
        $calculator->tip = $request->tip;
        $calculator->data_receptie = $request->data_receptie; 

        $calculator->save(); 

        return redirect()->route('calculator');

    }
}
