<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests; 
use App\Utilizator;

class PagesController extends Controller
{
    public function home() {

    	return view('pages.home');
    } 

    public function getViewLista() {

        return view('pages.lista');
    }

    public function getInregistrare() {

    	return view('pages.inregistrare');
    } 

    public function postInregistrare(Request $request) {

    	  $utilizator = new Utilizator(); 
    	  $utilizator->firstname = $request->firstname; 
        $utilizator->lastname = $request->lastname; 
        $utilizator->email = $request->email; 
        $utilizator->password = $request->password; 
        
        if($request->hasFile('file')){ 
        $file = $request->file('file'); 
        $file->move('uploads', $file->getClientOriginalName());  
        $utilizator->image_name = $file->getClientOriginalName();
        } 

        $utilizator->save(); 

        return redirect()->route('lista')->with('Success','Utilizator adaugat');
    }

  public function getLista(){ 
        
          $utilizatori = Utilizator::get();

        return view("pages.lista", compact('utilizatori'));


  } 
  public function destroy($id){ 
        $utilizatori = Utilizator::findOrFail($id);  

        $utilizatori->delete(); 
        return redirect()->route('lista');


  }
  public function getEditUser(Request $request, $id){ 
        $utilizator = Utilizator::findOrFail($id); 


        return view('pages.edit', compact('utilizator'));

  } 

    public function postEditUser(Request $request, $id){ 

        $this->validate($request, [
                  'firstname'=>'required', 
                  'lastname'=>'required', 
                  'email'=>'required|email',
                  'password'=>'required',  
                  'file' => 'required'
          ]);


        $utilizator = Utilizator::findOrFail($id); 
        $utilizator->firstname = $request->firstname; 
        $utilizator->lastname = $request->lastname; 
        $utilizator->email = $request->email; 
        $utilizator->password = $request->password; 
        
        if($request->hasFile('file')){
        $file = $request->file('file'); 
        $file->move('uploads', $file->getClientOriginalName());  
        $utilizator->image_name = $file->getClientOriginalName();
        } 

        $utilizator->save(); 

        return redirect()->route('lista');
  }

    public function getButoane() {
        return view('pages.interfataselectare');

    }

   public function getProfile($id) {
      $utilizator = Utilizator::findOrFail($id); 


      return view('pages.profil', compact('utilizator'));
   }

}
