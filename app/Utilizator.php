<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilizator extends Model
{ 
	public function calculator() 
	{
		return $this->hasMany('App\Calculator');
	}

	public function imagine() 
	{	
		$image = asset('Uploads/'.$this->image_name); 
		$path = public_path('Uploads/'.$this->image_name);
		return file_exists($path) && !is_dir($path) ? $image : asset('Uploads/avatar.jpg');
	}
}
