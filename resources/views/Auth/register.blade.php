@extends('pages.layouts.masters.main') 
 
@section('page-content')  
	
	<div class="container"> 

	@include('pages.layouts.partials.nav')

      <form class="form-signup">
        <h2 class="form-signin-heading">Sign up</h2> 
        <label for="inputFirstName" class="sr-only">Firstnamne</label>
        <input type="text" id="firstname" class="form-control" placeholder="firstname" required autofocus> 
        
        <label for="inputLastname" class="sr-only">Lastname</label>
        <input type="text" id="lastname" class="form-control" placeholder="lastname" required autofocus> 

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus> \
          
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        
          
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form>

    </div> <!-- /container -->


@endsection

