@extends('pages.layouts.masters.main') 
@section('content')
@if(count($errors)>0)  
    <div class="display erori"> 
    <ul> 
        @foreach($errors->all() as $err) 
            <li>{{$err}}</li>
    </ul>
        @endforeach
    </div> 
@endif


<form class="form-signup" action="{{route('postEditUser',['id' =>$utilizator->id])}}" method="post" enctype="multipart/form-data"> 
 {!! csrf_field() !!}
        <h2 class="form-signin-heading">EDITARE DATE</h2> 
        <label for="firstname" class="sr-only">Firstnamne</label>
        <input type="text" id="firstname" name="firstname" value="{{ $utilizator->firstname }}" class="form-control" placeholder="firstname" > <br><br>
        
        <label for="firstname" class="sr-only">Lastname</label>
        <input type="text" id="lastname" name="lastname" value="{{ $utilizator->lastname }}" class="form-control" placeholder="lastname" > <br><br>

        <label for="email" class="sr-only">Email address</label>
        <input type="email" id="email" name="email" value="{{ $utilizator->email }}" class="form-control" placeholder="email" > <br><br>
          
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" value="{{ $utilizator->password }}" class="form-control" placeholder="assword" > <br><br> 
        <label for="file">Upload profile picture</label>
        <input type="file" name="file" id = "file"><br><br>
        
          
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
      </form>