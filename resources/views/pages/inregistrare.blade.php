@extends('pages.layouts.masters.main') 
@section('content')

 <form class="form-signup" action="{{route('post_inregistrare')}}" method="post"> 
 {!! csrf_field() !!}
        <h2 class="form-signin-heading">Sign up</h2> 
        <label for="firstname" class="sr-only">Firstnamne</label>
        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="firstname" title = "This field is required!" > 
        
        <label for="firstname" class="sr-only">Lastname</label>
        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="lastname" > 

        <label for="email" class="sr-only">Email address</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="email" > 
          
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="password" > <br><br>

        <label for="file">Upload profile picture</label>
        <input type="file" name="file" id = "file"><br><br>
        
          
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form> 
@stop