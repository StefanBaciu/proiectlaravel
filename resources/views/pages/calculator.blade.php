@extends('pages.layouts.masters.main') 
@section('content')
{!!Form::open(array(
        'route'=>'calculator', 
        'method'=>'get'))
        !!}
{!!Form::select('user_id', $utilizatori, null, ['placeholder' => 'Select...'])!!} 
{!!Form::submit('Select user')!!}
{!!Form::close()!!}
<table>
	<tr> 
		<th>User</th>
		<th>MODEL</th> 
		<th>TIP</th> 
		<th>Data Receptie</th> 
		
	</tr> 

	@foreach($calculatoare as $calculator)
	<tr>
		<td>{{$calculator->utilizator->firstname}}</td>
		<td>{{$calculator->model}}</td>
		<td>{{$calculator->tip}}</td>
		<td>{{$calculator->data_receptie}}</td>  
		<td><a href = "{{route('getEditCalculator',$calculator->id)}}"><button>edit</button></a></td>

	</tr> 
	@endforeach
</table><br>  
<a href = "{{route('get_insertcalculator')}}"><button>Insert calculator</button></a>


@stop