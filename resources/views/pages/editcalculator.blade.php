@extends('pages.layouts.masters.main') 
@section('content')
<h2>Edit the owned computer</h2>
{!!Form::open(array(
        'route'=>['postEditCalculator', $calculator->id], 
        'method'=>'post'))
        !!}

    {!!Form::label('model','Model')!!} 
    {!!Form::text('model',"$calculator->model")!!}<br>  

    {!!Form::label('tip','Tip:')!!} <br>
    {!!Form::radio('tip', 'laptop', $calculator->tip == "laptop"? true:false)!!} laptop <br>
    {!!Form::radio('tip', 'desktop', $calculator->tip == "desktop"? true:false)!!} desktop <br>

    {!!Form::label('data_receptie', 'Data receptie')!!} 
    {!!Form::text('data_receptie',"$calculator->data_receptie")!!}<br>

    {!!Form::submit('save')!!}   

{!!Form::close()!!}


@stop